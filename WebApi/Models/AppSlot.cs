﻿// ======================================================
// <copyright file="AppSlot.cs" company="CSC">
// Copyright (c) 2016 All Rights Reserved
// </copyright>
// <author>Gopi Katari</author>
// <date>03/15/2016 10:00:00 AM </date>
// <summary>Class representing a AppSlot entity</summary>
//======================================================
namespace WebApi.Models
{
    public class AppSlot
    {
        public string SlotID { get; set; }
        public string SlotTime { get; set; }
    }
}